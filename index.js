const cards = document.querySelectorAll(".card");

const start = document.getElementById("start")
const moves = document.getElementById("move");
const timer = document.getElementById("timer");

const cardsContainer = document.querySelector(".card-container");

let previousCard;
let flag = false;
let numberOfMatches = 0;
let setIntervalId;

start.addEventListener("click", startGame);

cardsContainer.addEventListener("click", shuffleEvent);

function startGame(event) {
    start.style.color = "#fff";
    if (flag === false) {
        setIntervalId = setInterval(() => {
            timer.innerText = Number(timer.innerText) + 1;
        }, 1000);
        flag = true;
    };
};


let runningCards = 0;


function shuffleEvent(event) {
    // console.log(flag, runningCards, event.target);

    if (event.target.classList.contains("card") && flag && runningCards < 2) {
        moves.innerText = Number(moves.innerText) + 1;
        if (event.target.classList.contains("rotateBack")) {
            event.target.classList.remove("rotateBack");
        };

        event.target.classList.add("rotateImage");
        event.target.firstElementChild.style.display = "block";

        runningCards++;

        if (previousCard === undefined) {
            previousCard = event.target;
            console.log(previousCard);
        } else if (matched(previousCard, event.target) === true) {
            console.log("card matched");

            previousCard = undefined;
            numberOfMatches++;

            runningCards = 0;

            if (numberOfMatches === 8) {
                console.log("all cards matched");
                clearInterval(setIntervalId);
            };

        } else {
            runningCards++;
            setTimeout(() => {

                previousCard.classList.remove("rotateImage");
                previousCard.classList.add("rotateBack");
                previousCard.firstElementChild.style.display = "none";

                event.target.classList.remove("rotateImage");
                event.target.classList.add("rotateBack");
                event.target.firstElementChild.style.display = "none";

                previousCard = undefined;
                runningCards = 0;

            }, 1000);
        }
    }
};


function matched(previousCard, curretCard) {
    if (previousCard.firstElementChild.getAttribute("name") === curretCard.firstElementChild.getAttribute("name")) {
        return true;
    } else {
        return false;
    }
};


const star = document.querySelector(".fa-star");
// console.log(star);

document.body.addEventListener("mouseover", function () {
    // star.style.animation = "movingTopToBottom 5s linear infinite";
    star.style.display = "block";
});
document.body.addEventListener("mouseout", function () {
    star.style.display = "none";
});